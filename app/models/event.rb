class Event < ActiveRecord::Base
  validates :title, :body, presence: :true

end
